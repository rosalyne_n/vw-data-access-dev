package com.vw.cn.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.vw.cn.domain.Budget;
import com.vw.cn.domain.CarModel;
import com.vw.cn.domain.CarSeries;
import com.vw.cn.domain.PurchaseOrder;
import com.vw.cn.domain.TestDrive;
/**
 * @author karthikeyan_v
 */
@Mapper
public interface CarDao
{
	@Insert(value = {"insert into VW_MODEL(CODE,NAME,SERIES_CODE,LANG, CURRENCY_CODE, RECOMMENDED_RETAIL_PRICE,DELETE_FLAG,UPDATE_TIME) values(#{code},#{model_name},#{series_code},#{lang},#{currency_code},#{price},#{delete_flag},#{update_time})"})
	void insertCarModel(CarModel carModel);

	@Update(value = {"update VW_MODEL set CODE=#{code},NAME=#{model_name},SERIES_CODE=#{series_code},LANG={lang},CURRENCY_CODE={currency_code},RECOMMENDED_RETAIL_PRICE=#{price},DELETE_FLAG=#{delete_flag},UPDATE_TIME=#{update_time} where ID=#{id}"})
	void updateCarModel(CarModel carModel); 

	@Select(value = {"select * from VW_MODEL WHERE ID=#{id} and DELETE_FLAG=0"})
	CarModel findCarModelById(long id);			

	@Select(value = {"select * from VW_MODEL WHERE DELETE_FLAG=0"})
	List<CarModel> findAllCarModels();	

	//@Delete(value = {"delete from CAR_MODEL where ID=#{id}"})
	@Update(value = {"update VW_MODEL set DELETE_FLAG=#{delete_flag} where ID=#{id}"})
	void deleteCarModel(CarModel carModel);
	
	//Test Drive
	@Insert(value = {"insert into VW_TEST_DRIVE(NAME,GENDER,MOBILE_NO,MODEL_CODE,DEALER_ID,DEALER_PROVINCE_CODE,DEALER_CITY_CODE,PLAN_BUY_TIME,STATUS,REGISTER_DATE,COMMENTS,UPDATED_BY,LAST_UPDATED_DATE) values(#{name},#{gender},#{mobile_no},#{model_code},#{dealer_id},#{dealer_province_code},#{dealer_city_code},#{plan_buy_time},#{status},#{register_date},#{comment},#{updated_by},#{last_updated_date})"})
	void insertTestDrive(TestDrive testDrive);

	@Update(value = {"update VW_TEST_DRIVE set NAME=#{name},GENDER=#{gender},MOBILE_NO=#{mobile_no},MODEL_CODE=#{model_code},DEALER_ID=#{dealer_id},DEALER_PROVINCE_CODE=#{dealer_province_code},DEALER_CITY_CODE=#{dealer_city_code},PLAN_BUY_TIME=#{plan_buy_time},STATUS=#{status},REGISTER_DATE=#{register_date},COMMENTS=#{comment},UPDATED_BY=#{updated_by},LAST_UPDATED_DATE=#{last_updated_date} where ID=#{id}"})
	void updateTestDrive(TestDrive testDrive);

	@Select(value = {"select * from VW_TEST_DRIVE WHERE ID=#{id}"})
	TestDrive findTestDriveById(long id);			

	@Select(value = {"select * from VW_TEST_DRIVE"})
	List<TestDrive> findAllTestDrives();	

	@Delete(value = {"delete from VW_TEST_DRIVE where ID=#{id}"})
	void deleteTestDrive(long id);
	
	//Purchase Order
	@Insert(value = {"insert into VW_PURCHASE_ORDER(NAME,GENDER,MOBILE_NO,MODEL_CODE,DEALER_ID,DEALER_PROVINCE_CODE,DEALER_CITY_CODE,ORDER_DATE,PLAN_BUY_TIME,DELETE_FLAG,STATUS,COMMENTS,UPDATED_BY,LAST_UPDATED_DATE) values(#{name},#{gender},#{mobile_no},#{model_code},#{dealer_id},#{dealer_province_code},#{dealer_city_code},#{order_date},#{plan_buy_time},#{delete_flag},#{status},#{comment},#{updated_by},#{last_updated_date})"})
	void insertPurchaseOrder(PurchaseOrder carModel);

	@Update(value = {"update VW_PURCHASE_ORDER set NAME=#{name},GENDER=#{gender},MOBILE_NO=#{mobile_no},MODEL_CODE=#{model_code},DEALER_ID=#{dealer_id},DEALER_PROVINCE_CODE=#{dealer_province_code},DEALER_CITY_CODE=#{dealer_city_code},ORDER_DATE=#{order_date},PLAN_BUY_TIME=#{plan_buy_time},DELETE_FLAG=#{delete_flag},STATUS=#{status},COMMENTS=#{comment},UPDATED_BY=#{updated_by},LAST_UPDATED_DATE=#{last_updated_date} where ID=#{id}"})
	void updatePurchaseOrder(PurchaseOrder carModel);

	@Select(value = {"select * from VW_PURCHASE_ORDER WHERE ID=#{id} and DELETE_FLAG=0"})
	PurchaseOrder findPurchaseOrderById(long id);			

	@Select(value = {"select * from VW_PURCHASE_ORDER WHERE DELETE_FLAG=0"})
	List<PurchaseOrder> findAllPurchaseOrders();	

	//@Delete(value = {"delete from CAR_MODEL where ID=#{id}"})
	@Update(value = {"update VW_PURCHASE_ORDER set DELETE_FLAG=#{delete_flag} where ID=#{id}"})
	void deletePurchaseOrder(PurchaseOrder order);
	
	@Select(value={"select * from VW_CAR_MODEL WHERE PRICE BETWEEN #{start,jdbcType=FLOAT} and #{end,jdbcType=FLOAT}"})
	List<CarModel> findBugetCarModels(Budget budget);
	
	@Insert(value = {"insert into VW_SERIES(CODE,NAME,LANG,DELETE_FLAG,UPDATE_TIME) values(#{code},#{name},#{lang},#{delete_flag},#{update_time})"})
	void insertCarSeries(CarSeries carSeries);
}