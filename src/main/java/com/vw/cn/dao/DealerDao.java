package com.vw.cn.dao;
/**
 * @author karthikeyan_v
 */
import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.vw.cn.domain.ContactType;
import com.vw.cn.domain.Dealer;
import com.vw.cn.domain.DealerContact;
import com.vw.cn.domain.Promotion;
import com.vw.cn.domain.ServiceRating;

@Mapper
public interface DealerDao
{
	@Insert(value = {"insert into VW_DEALER(DECLARED_CITY,DECLARED_PROVINCE,DECLARED_DISTRICT,COMPANY_NAME,LOCATION_CITY,REGISTERED_CAPITAL,INDUSTRY_PERIOD,BRAND_OPERATION,PREFERRED_SITE_ADDRESS,NETWORK_TYPE_CODE,FORM_STATUS,UNIQUE_CODE,USER_ID,REGISTRATION_DATE,DELETE_FLAG,UPDATE_TIME,LATTITUDE,LONGITUDE,NO_OF_UPDATES,WEBSITE_URL) values(#{declared_city},#{declared_province},#{declared_district},#{company_name},#{location_city},#{registered_capital},#{industry_period},#{brand_operation},#{preferred_site_address},#{network_type_code},#{form_status},#{unique_code},#{user_id},#{registration_date},#{delete_flag},#{update_time},#{latitude},#{longitude},#{no_of_updates},#{website_url})"})
	void insertDealer(Dealer dealerRegistration);

	@Update(value = {"update VW_DEALER set DECLARED_CITY=#{declared_city},DECLARED_PROVINCE=#{declared_province},DECLARED_DISTRICT=#{declared_district},COMPANY_NAME=#{company_name},LOCATION_CITY=#{location_city},REGISTERED_CAPITAL=#{registered_capital},INDUSTRY_PERIOD=#{industry_period},BRAND_OPERATION=#{brand_operation},PREFERRED_SITE_ADDRESS=#{preferred_site_address},NETWORK_TYPE_CODE=#{network_type_code},FORM_STATUS=#{form_status},UNIQUE_CODE=#{unique_code},USER_ID=#{user_id},REGISTRATION_DATE=#{registration_date},UPDATE_TIME=#{update_time},LATTITUDE=#{latitude},LONGITUDE=#{longitude},NO_OF_UPDATES=#{no_of_updates},WEBSITE_URL=#{website_url} where ID=#{id}"})
	void updateDealer(Dealer dealerRegistration);

	@Select(value = {"select * from VW_DEALER WHERE ID=#{id} and DELETE_FLAG=0"})
	Dealer findDealerById(long id);

	@Select(value = {"select * from VW_DEALER WHERE UNIQUE_CODE=#{unique_code} and DELETE_FLAG=0"})
	Dealer findDealerByCode(String unique_code);

	@Select(value = {"select * from VW_DEALER and DELETE_FLAG=0"})
	List<Dealer> findAllDealers();	

	//@Delete(value = {"delete from VW_DEALER_REGISTRATION where ID=#{id}"})
	@Update(value = {"update VW_DEALER set DELETE_FLAG=#{delete_flag},UPDATE_TIME=#{update_time} where ID=#{id}"})
	void deleteDealer(Dealer dealer);

	//## Dealer Contact
	@Insert(value = {"insert into VW_DEALER_CONTACT(NAME,PHONE,EMAIL,JOB_TITLE,CITY,LAST_MODIFIED_BY,DEALER_ID,CREATED_DATETIME,LAST_MODIFIED_DATETIME,CONTACT_TYPE) values(#{name},#{phone},#{email},#{job_title},#{city},#{last_modified_by},#{dealer_id},#{created_date},#{last_modified_date},#{contact_type})"})
	void insertDealerContact(DealerContact dealerRegistrationContact);

	@Update(value = {"update VW_DEALER_CONTACT set NAME=#{name},PHONE=#{phone},EMAIL=#{email},JOB_TITLE=#{job_title},CITY=#{city},LAST_MODIFIED_BY=#{last_modified_by},DEALER_ID=#{dealer_id},CREATED_DATETIME=#{created_date},LAST_MODIFIED_DATETIME=#{last_modified_date},CONTACT_TYPE=#{contact_type} where ID=#{id}"})
	void updateDealerContact(DealerContact dealerRegistrationContact);

	@Select(value = {"select * from VW_DEALER_CONTACT WHERE ID=#{id}"})
	DealerContact findDealerContactById(long id);

	@Select(value = {"select * from VW_DEALER_CONTACT"})
	List<DealerContact> findAllDealerContacts();	

	@Delete(value = {"delete from VW_DEALER_CONTACT where ID=#{id}"})
	void deleteDealerContact(long id);	

	//## Promotion
	@Insert(value = {"insert into VW_DEALER_PROMOTION(DEALER_ID,PROMOTIONAL_BANNER_IMAGE_URL,PROMOTION_PAGE_URL,CREATED_DATE,VALID_FROM,VALID_TILL) values(#{dealer_id},#{promotional_banner_image_url},#{promotion_page_url},#{created_date},#{valid_from},#{valid_till})"})
	void insertPromotion(Promotion promotion);

	@Update(value = {"update VW_DEALER_PROMOTION set DEALER_ID=#{dealer_id},PROMOTIONAL_BANNER_IMAGE_URL=#{promotional_banner_image_url},PROMOTION_PAGE_URL=#{promotion_page_url},CREATED_DATE=#{created_date},VALID_FROM=#{valid_from},VALID_TILL=#{valid_till} where ID=#{id}"})
	void updatePromotion(Promotion promotion);

	@Select(value = {"select * from VW_DEALER_PROMOTION WHERE ID=#{id}"})
	Promotion findPromotionById(long id);

	@Select(value = {"select * from VW_DEALER_PROMOTION"})
	List<Promotion> findAllPromotions();	

	@Delete(value = {"delete from VW_DEALER_PROMOTION where ID=#{id}"})
	void deletePromotion(long id);		

	//## Service Rating
	@Insert(value = {"insert into VW_DEALER_RATING(USER_ID,DEALER_ID,SERVICE_RATING,RATED_ON,DESCRIPTION) values(#{user_id},#{dealer_id},#{service_rating},#{rated_on},#{description})"})
	void insertServiceRating(ServiceRating serviceRating);

	@Update(value = {"update VW_DEALER_RATING set USER_ID=#{user_id},DEALER_ID=#{dealer_id},SERVICE_RATING=#{service_rating},RATED_ON=#{rated_on},DESCRIPTION=#{description} where ID=#{id}"})
	void updateServiceRating(ServiceRating serviceRating);

	@Select(value = {"select * from VW_DEALER_RATING WHERE ID=#{id}"})
	ServiceRating findServiceRatingById(long id);

	@Select(value = {"select * from VW_DEALER_RATING"})
	List<ServiceRating> findAllServiceRatings();	

	@Delete(value = {"delete from VW_DEALER_RATING where ID=#{id}"})
	void deleteServiceRating(long id);	

	//## Contact Type
	@Insert(value = {"insert into VW_CONTACT_TYPE(SALES_HOTLINE,CUSTOMER_HOTLINE,AFTER_SALES,UPDATE_TIME,DELETE_FLAG) values(#{sales_hotline},#{customer_hotline},#{after_sales},#{update_time},#{delete_flag})"})
	void insertContactType(ContactType contactType);

	@Update(value = {"update VW_CONTACT_TYPE set SALES_HOTLINE=#{sales_hotline},CUSTOMER_HOTLINE=#{customer_hotline},AFTER_SALES=#{after_sales},UPDATE_TIME=#{update_time},DELETE_FLAG=#{delete_flag} where ID=#{id}"})
	void updateContactType(ContactType contactType);

	@Select(value = {"select * from VW_CONTACT_TYPE WHERE ID=#{id}"})
	ContactType findContactTypeById(long id);

	@Select(value = {"select * from VW_CONTACT_TYPE"})
	List<ContactType> findAllContactTypes();	

	@Delete(value = {"delete from VW_CONTACT_TYPE where ID=#{id}"})
	void deleteContactType(long id);	

	//Search Dealer
	@Select(value = {"select "
			+ "d.ID as id,d.DECLARED_CITY as city_code,d.DECLARED_PROVINCE as province_code,d.LATTITUDE as latitude,d.LONGITUDE as longitude,d.PREFERRED_SITE_ADDRESS as formatted_address,d.WEBSITE_URL as website_url,d.NO_OF_UPDATES as no_of_updates, d.NETWORK_TYPE_CODE as dealer_type,dc.NAME as name, "
			+ "c.NAME as city_name, "
			+ "p.NAME as province_name, "
			+ "s.SERVICE_RATING as service_rating, "
			+ "pro.PROMOTIONAL_BANNER_IMAGE_URL as promotional_banner_image_url,pro.PROMOTION_PAGE_URL as promotion_page_url, "
			+ "ct.SALES_HOTLINE as contact_sales_hotline,ct.CUSTOMER_HOTLINE as contact_customer_hotline,ct.AFTER_SALES as contact_after_sales "
			+ "FROM VW_DEALER d "
			+ "JOIN VW_DEALER_CONTACT dc on dc.DEALER_ID=d.ID "
			+ "JOIN VW_CITY c on c.CODE=d.DECLARED_CITY "
			+ "JOIN VW_PROVINCE p on p.CODE=d.DECLARED_PROVINCE "
			+ "JOIN VW_DEALER_RATING s on s.DEALER_ID=d.ID "
			+ "JOIN VW_DEALER_PROMOTION pro on pro.DEALER_ID=d.ID "
			+ "JOIN VW_CONTACT_TYPE ct on ct.ID=dc.CONTACT_TYPE "
			+ "where "
		//	+ "d.DECLARED_CITY=#{d.declared_city} and d.DECLARED_PROVINCE=#{d.declared_province} and d.NETWORK_TYPE_CODE=#{d.network_type_code}; "})
		+ "d.DECLARED_CITY='001' and d.DECLARED_PROVINCE='P1' and d.NETWORK_TYPE_CODE='RE1';"})
	List<Dealer> searchDealer(String province_code,String city_code,String dealer_type);	
}
