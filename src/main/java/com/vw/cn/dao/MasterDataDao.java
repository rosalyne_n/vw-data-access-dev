package com.vw.cn.dao;
/**
 * @author karthikeyan_v
 */
import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.vw.cn.domain.City;
import com.vw.cn.domain.Currency;
import com.vw.cn.domain.District;
import com.vw.cn.domain.Language;
import com.vw.cn.domain.Province;
import com.vw.cn.domain.RecruitmentNetworkType;
import com.vw.cn.domain.Role;

@Mapper
public interface MasterDataDao
{
	//Role

	@Insert(value = {"insert into VW_ROLE(NAME,DESCRIPTION,DELETE_FLAG,UPDATE_TIME) values(#{name},#{description},#{delete_flag},#{update_time})"})
	void insertRole(Role role);

	@Update(value = {"update VW_ROLE set NAME=#{name},DESCRIPTION=#{description},DELETE_FLAG=#{delete_flag},UPDATE_TIME=#{update_time} where ID=#{id}"})
	void updateRole(Role role);

	@Select(value = {"select * from VW_ROLE WHERE ID=#{id}"})
	Role findRoleById(long id);

	@Select(value = {"select * from VW_ROLE"})
	List<Role> findAllRoles();	

	@Delete(value = {"delete from VW_ROLE where ID=#{id}"})
	void deleteRole(long id);

	//Lang

	@Insert(value = {"insert into VW_LANG(CODE,NAME,DELETE_FLAG,UPDATE_TIME) values(#{code},#{name},#{delete_flag},#{update_time})"})
	void insertLanguage(Language lang);

	@Update(value = {"update VW_LANG set CODE=#{code},NAME=#{name},DELETE_FLAG=#{delete_flag},UPDATE_TIME=#{update_time} where ID=#{id}"})
	void updateLanguage(Language lang);

	@Select(value = {"select * from VW_LANG WHERE ID=#{id}"})
	Language findLanguageById(long id);

	@Select(value = {"select * from VW_LANG WHERE CODE=#{code}"})
	Language findLanguageByCode(String code);

	@Select(value = {"select * from VW_LANG"})
	List<Language> findAllLanguages();	

	@Delete(value = {"delete from VW_LANG where ID=#{id}"})
	void deleteLanguage(long id);

	//Province

	@Insert(value = {"insert into VW_PROVINCE(CODE,NAME,LANG_CODE,DELETE_FLAG,UPDATE_TIME) values(#{code},#{name},#{lang_code},#{delete_flag},#{update_time})"})
	void insertProvince(Province province);

	@Update(value = {"update VW_PROVINCE set CODE=#{code},NAME=#{name},LANG_CODE=#{lang_code},DELETE_FLAG=#{delete_flag},UPDATE_TIME=#{update_time} where ID=#{id}"})
	void updateProvince(Province province);

	@Select(value = {"select p.ID,p.NAME,p.LANG_CODE,p.CODE,l.ID as LANG_ID,l.NAME as LANG_NAME from VW_PROVINCE p left outer join VW_LANG l on p.LANG_CODE=l.ID WHERE p.ID=#{id}"})
	Province findProvinceById(long id);

	@Select(value = {"select * from VW_PROVINCE WHERE CODE=#{code}"})
	Province findProvinceByCode(String code);

	@Select(value = {"select p.ID,p.NAME,p.LANG_CODE,l.ID as LANG_ID,l.NAME as LANG_NAME from VW_PROVINCE p left outer join VW_LANG l on p.LANG_CODE=l.ID"})
	List<Province> findAllProvinces();	

	@Delete(value = {"delete from VW_PROVINCE where ID=#{id}"})
	void deleteProvince(long id);

	//City

	@Insert(value = {"insert into VW_CITY(CODE,NAME,PROVINCE_CODE,LANG_CODE,DELETE_FLAG,UPDATE_TIME) values(#{code},#{name},#{province_code},#{lang_code},#{delete_flag},#{update_time})"})
	void insertCity(City city);

	@Update(value = {"update VW_CITY set CODE=#{code},NAME=#{name},PROVINCE_CODE=#{province_code},LANG_CODE=#{lang_code},DELETE_FLAG=#{delete_flag},UPDATE_TIME=#{update_time} where ID=#{id}"})
	void updateCity(City city);

	@Select(value = {"select * from VW_CITY WHERE ID=#{id}"})
	City findCityById(long id);

	@Select(value = {"select * from VW_CITY WHERE CODE=#{code}"})
	City findCityByCode(String code);

	@Select(value = {"select * from VW_CITY"})
	List<City> findAllCities();	

	@Delete(value = {"delete from VW_CITY where ID=#{id}"})
	void deleteCity(long id);

	//District

	@Insert(value = {"insert into VW_DISTRICT(CODE,NAME,CITY_CODE,LANG_CODE,DELETE_FLAG,UPDATE_TIME) values(#{code},#{name},#{city_code},#{lang_code},#{delete_flag},#{update_time})"})
	void insertDistrict(District district);

	@Update(value = {"update VW_DISTRICT set CODE=#{code},NAME=#{name},CITY_CODE=#{city_code},LANG_CODE=#{lang_code},DELETE_FLAG=#{delete_flag},UPDATE_TIME=#{update_time} where ID=#{id}"})
	void updateDistrict(District district);

	@Select(value = {"select * from VW_DISTRICT WHERE ID=#{id}"})
	District findDistrictById(long id);

	@Select(value = {"select * from VW_DISTRICT WHERE CODE=#{code}"})
	District findDistrictByCode(String code);

	@Select(value = {"select * from VW_DISTRICT"})
	List<District> findAllDistricts();	

	@Delete(value = {"delete from VW_DISTRICT where ID=#{id}"})
	void deleteDistrict(long id);

	//RecruitmentNetworkType

	@Insert(value = {"insert into VW_RECRUITMENT_NETWORK_TYPE(CODE,NAME,LANG_CODE,DELETE_FLAG,UPDATE_TIME) values(#{code},#{name},#{lang_code},#{delete_flag},#{update_time})"})
	void insertRecruitmentNetworkType(RecruitmentNetworkType recruitmentNetwork);

	@Update(value = {"update VW_RECRUITMENT_NETWORK_TYPE set CODE=#{code},NAME=#{name},LANG_CODE=#{lang_code},DELETE_FLAG=#{delete_flag},UPDATE_TIME=#{update_time} where ID=#{id}"})
	void updateRecruitmentNetworkType(RecruitmentNetworkType recruitmentNetwork);

	@Select(value = {"select * from VW_RECRUITMENT_NETWORK_TYPE WHERE ID=#{id}"})
	RecruitmentNetworkType findRecruitmentNetworkTypeById(long id);

	@Select(value = {"select * from VW_RECRUITMENT_NETWORK_TYPE WHERE CODE=#{code}"})
	RecruitmentNetworkType findRecruitmentNetworkTypeByCode(String code);

	@Select(value = {"select * from VW_RECRUITMENT_NETWORK_TYPE"})
	List<RecruitmentNetworkType> findAllRecruitmentNetworkTypes();	

	@Delete(value = {"delete from VW_RECRUITMENT_NETWORK_TYPE where ID=#{id}"})
	void deleteRecruitmentNetworkType(long id);	

	//Currency

	@Insert(value = {"insert into VW_CURRENCY(CODE,NAME,SYMBOL,DELETE_FLAG,UPDATE_TIME) values(#{code},#{name},#{symbol},#{delete_flag},#{update_time})"})
	void insertCurrency(Currency currency);

	@Update(value = {"update VW_CURRENCY set CODE=#{code},NAME=#{name},SYMBOL=#{symbol},DELETE_FLAG=#{delete_flag},UPDATE_TIME=#{update_time} where ID=#{id}"})
	void updateCurrency(Currency currency);

	@Select(value = {"select * from VW_CURRENCY WHERE ID=#{id}"})
	Currency findCurrencyById(long id);

	@Select(value = {"select * from VW_CURRENCY"})
	List<Currency> findAllCurrencys();	

	@Delete(value = {"delete from VW_CURRENCY where ID=#{id}"})
	void deleteCurrency(long id);
}
