package com.vw.cn.domain;

import java.util.Date;

/**
 * @author karthikeyan_v
 */
public class Dealer {

	private long id;

	private String declared_city;

	private String declared_province;

	private String declared_district;

	private String company_name;

	private String location_city;

	private String registered_capital;

	private long industry_period;

	private String brand_operation;

	private String preferred_site_address;

	private String network_type_code;

	private String form_status;

	private String unique_code;

	private long user_id;

	private Date registration_date;  

	private boolean delete_flag;

	private Date update_time;

	private float latitude;

	private float longitude;

	private long no_of_updates;

	private String website_url;

	//private City city;

	//private Province province;

	//private District district;

	//private RecruitmentNetworkType recruitmentNetworkType;

	//private User user;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDeclared_city() {
		return declared_city;
	}

	public void setDeclared_city(String declared_city) {
		this.declared_city = declared_city;
	}

	public String getDeclared_province() {
		return declared_province;
	}

	public void setDeclared_province(String declared_province) {
		this.declared_province = declared_province;
	}

	public String getDeclared_district() {
		return declared_district;
	}

	public void setDeclared_district(String declared_district) {
		this.declared_district = declared_district;
	}

	public String getCompany_name() {
		return company_name;
	}

	public void setCompany_name(String company_name) {
		this.company_name = company_name;
	}

	public String getLocation_city() {
		return location_city;
	}

	public void setLocation_city(String location_city) {
		this.location_city = location_city;
	}

	public String getRegistered_capital() {
		return registered_capital;
	}

	public void setRegistered_capital(String registered_capital) {
		this.registered_capital = registered_capital;
	}

	public long getIndustry_period() {
		return industry_period;
	}

	public void setIndustry_period(long industry_period) {
		this.industry_period = industry_period;
	}

	public String getBrand_operation() {
		return brand_operation;
	}

	public void setBrand_operation(String brand_operation) {
		this.brand_operation = brand_operation;
	}

	public String getPreferred_site_address() {
		return preferred_site_address;
	}

	public void setPreferred_site_address(String preferred_site_address) {
		this.preferred_site_address = preferred_site_address;
	}

	public String getNetwork_type_code() {
		return network_type_code;
	}

	public void setNetwork_type_code(String network_type_code) {
		this.network_type_code = network_type_code;
	}

	public String getForm_status() {
		return form_status;
	}

	public void setForm_status(String form_status) {
		this.form_status = form_status;
	}

	public String getUnique_code() {
		return unique_code;
	}

	public void setUnique_code(String unique_code) {
		this.unique_code = unique_code;
	}

	public long getUser_id() {
		return user_id;
	}

	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}

	public Date getRegistration_date() {
		return registration_date;
	}

	public void setRegistration_date(Date registration_date) {
		this.registration_date = registration_date;
	}

	public boolean isDelete_flag() {
		return delete_flag;
	}

	public void setDelete_flag(boolean delete_flag) {
		this.delete_flag = delete_flag;
	}

	public Date getUpdate_time() {
		return update_time;
	}

	public void setUpdate_time(Date update_time) {
		this.update_time = update_time;
	}

	public float getLatitude() {
		return latitude;
	}

	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}

	public float getLongitude() {
		return longitude;
	}

	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}

	public long getNo_of_updates() {
		return no_of_updates;
	}

	public void setNo_of_updates(long no_of_updates) {
		this.no_of_updates = no_of_updates;
	}

	public String getWebsite_url() {
		return website_url;
	}

	public void setWebsite_url(String website_url) {
		this.website_url = website_url;
	}		
}