package com.vw.cn.domain;

import java.util.Date;
/**
 * @author karthikeyan_v
 */
public class ServiceRating {

	private long id;

	private long user_id;

	private long dealer_id;	

	private long service_rating;

	private Date rated_on;

	private String description;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getUser_id() {
		return user_id;
	}

	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}

	public long getDealer_id() {
		return dealer_id;
	}

	public void setDealer_id(long dealer_id) {
		this.dealer_id = dealer_id;
	}

	public long getService_rating() {
		return service_rating;
	}

	public void setService_rating(long service_rating) {
		this.service_rating = service_rating;
	}

	public Date getRated_on() {
		return rated_on;
	}

	public void setRated_on(Date rated_on) {
		this.rated_on = rated_on;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
