package com.vw.cn.domain;

public class Budget {
private float downpayment;
private float emi;
private float start;
private float end;
public Float getStart() {
	return start;
}
public void setStart(Float start) {
	this.start = start;
}
public Float getEnd() {
	return end;
}
public void setEnd(Float end) {
	this.end = end;
}
private int tenure;
public float getDownpayment() {
	return downpayment;
}
public void setDownpayment(float downpayment) {
	this.downpayment = downpayment;
}
public float getEmi() {
	return emi;
}
public void setEmi(float emi) {
	this.emi = emi;
}
public int getTenure() {
	return tenure;
}
public void setTenure(int tenure) {
	this.tenure = tenure;
}
}
