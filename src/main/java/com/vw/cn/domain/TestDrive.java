package com.vw.cn.domain;
/**
 * @author Harihara Subramanian
 */
import java.util.Date;

public class TestDrive {

	private long id;

	private String name;

	private String gender;

	private String mobile_no;

	private String model_code;

	private long dealer_id;

	private String dealer_province_code;

	private String dealer_city_code;

	private String plan_buy_time;

	private String status;

	private Date register_date;

	private String comment;

	private long updated_by;

	private Date last_updated_date;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMobile_no() {
		return mobile_no;
	}

	public void setMobile_no(String mobile_no) {
		this.mobile_no = mobile_no;
	}

	
	public String getModel_code() {
		return model_code;
	}

	public void setModel_code(String model_code) {
		this.model_code = model_code;
	}

	public long getDealer_id() {
		return dealer_id;
	}

	public void setDealer_id(long dealer_id) {
		this.dealer_id = dealer_id;
	}

	public String getDealer_province_code() {
		return dealer_province_code;
	}

	public void setDealer_province_code(String dealer_province_code) {
		this.dealer_province_code = dealer_province_code;
	}

	public String getDealer_city_code() {
		return dealer_city_code;
	}

	public void setDealer_city_code(String dealer_city_code) {
		this.dealer_city_code = dealer_city_code;
	}

	public String getPlan_buy_time() {
		return plan_buy_time;
	}

	public void setPlan_buy_time(String plan_buy_time) {
		this.plan_buy_time = plan_buy_time;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getRegister_date() {
		return register_date;
	}

	public void setRegister_date(Date register_date) {
		this.register_date = register_date;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public long getUpdated_by() {
		return updated_by;
	}

	public void setUpdated_by(long updated_by) {
		this.updated_by = updated_by;
	}

	public Date getLast_updated_date() {
		return last_updated_date;
	}

	public void setLast_updated_date(Date last_updated_date) {
		this.last_updated_date = last_updated_date;
	}	
}
