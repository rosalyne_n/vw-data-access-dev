package com.vw.cn.domain;

import java.util.Date;

public class ContactType {

	private long id;	

	private String sales_hotline;

	private String customer_hotline;

	private String after_sales;

	private Date update_time;

	private boolean delete_flag;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSales_hotline() {
		return sales_hotline;
	}

	public void setSales_hotline(String sales_hotline) {
		this.sales_hotline = sales_hotline;
	}

	public String getCustomer_hotline() {
		return customer_hotline;
	}

	public void setCustomer_hotline(String customer_hotline) {
		this.customer_hotline = customer_hotline;
	}

	public String getAfter_sales() {
		return after_sales;
	}

	public void setAfter_sales(String after_sales) {
		this.after_sales = after_sales;
	}

	public Date getUpdate_time() {
		return update_time;
	}

	public void setUpdate_time(Date update_time) {
		this.update_time = update_time;
	}

	public boolean isDelete_flag() {
		return delete_flag;
	}

	public void setDelete_flag(boolean delete_flag) {
		this.delete_flag = delete_flag;
	}	
}