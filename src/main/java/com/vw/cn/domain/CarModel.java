package com.vw.cn.domain;
/**
 * @author Harihara Subramanian
 */
import java.util.Date;

public class CarModel {

	private long id;
	
	private String code;

	private String model_name;
	
	private String series_code;
	
	private String lang;
	
	private String currency_code;

	private Float price;

	private char delete_flag;

	private Date update_time;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getModel_name() {
		return model_name;
	}

	public void setModel_name(String model_name) {
		this.model_name = model_name;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public Date getUpdate_time() {
		return update_time;
	}

	public void setUpdate_time(Date update_time) {
		this.update_time = update_time;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getSeries_code() {
		return series_code;
	}

	public void setSeries_code(String series_code) {
		this.series_code = series_code;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getCurrency_code() {
		return currency_code;
	}

	public void setCurrency_code(String currency_code) {
		this.currency_code = currency_code;
	}

	public char getDelete_flag() {
		return delete_flag;
	}
	
	public void setDelete_flag(char delete_flag){
		this.delete_flag=delete_flag;
	}

		
}
